; Copyright 1997 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

; asmutils.s

        AREA    |main|, CODE, READONLY

        EXPORT  |svcarmid|
        EXPORT  |svccopy|
        EXPORT  |svcpeek|
        EXPORT  |svcpoke|
        EXPORT  |svcsetROML1PT|

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System

; uint32 svcarmid(void)
svcarmid
        SWI     XOS_EnterOS
        MRC     p15,0,r0,c0,c0,0
        TEQP    pc, #0
        MOV     r0, r0
        MOVS    pc, r14

; void svccopy(uint32 *src,uint32 *dest,int bytes)
svccopy
        SWI     XOS_EnterOS
copyloop
        LDR     r3, [r0], #4
        STR     r3, [r1], #4
        SUBS    r2, r2, #4
        BNE     copyloop
        TEQP    pc, #0
        MOV     r0, r0
        MOVS    pc, r14

; uint32 svcpeek(uint32 *addr)
svcpeek
        SWI     XOS_EnterOS
        LDR     r0, [r0]
        TEQP    pc, #0
        MOV     r0, r0
        MOVS    pc, r14

; void svcpoke(uint32 *addr, uint32 val)
svcpoke
        SWI     XOS_EnterOS
        STR     r1, [r0]
        TEQP    pc, #0
        MOV     r0, r0
        MOVS    pc, r14

; void svcsetROML1PT(uint32 *L1PTaddr, uint32 *L1PTvals, int count, void *flushfn)
svcsetROML1PT
        SWI     XOS_EnterOS
        Push    "r0-r4, r14"
        MOV     r4,pc
        ORR     r3,r4,#I_bit
        TEQP    r3,#0             ;disable interrupts
10
        LDR     r3, [r1], #4
        STR     r3, [r0], #4
        SUBS    r2, r2, #1
        BNE     %BT10

        MOV     lr, pc
        LDR     pc, [sp, #3*4]    ;flush cache(s) and TLB(s) without recourse to ROM

        TEQP    r4,#0             ;restore IRQ state
        MOV     r0, r0
        Pull    "r0-r4, r14"
        TEQP    pc, #0
        MOV     r0, r0
        MOVS    pc, r14

        END

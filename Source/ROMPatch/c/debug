/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* debug.c */

#include "defs.h"
#include "debug.h"

#if DEBUG

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "Global/OSRSI6.h"

/* ------------------------------------------------------------------------ */

void dbtrace_patchhits(int *romsection,int *romlpage,int *rompage)
{
  int i,j;
  static char *sym = ".123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ**";

  fprintf(stderr,"section hits:");
  for (i=0; i<ROMsections(ROMsize); i++)
  {
    if ((i & 63) == 0) fprintf(stderr,"\n");
    fprintf(stderr,"%c",sym[romsection[i]]);
  }
  fprintf(stderr,"\nlarge page hits:");
  for (i=0; i<ROMlpages(ROMsize); i++)
  {
    if ((i & 63) == 0) fprintf(stderr,"\n");
    fprintf(stderr,"%c",sym[romlpage[i]]);
  }
  fprintf(stderr,"\npage hits:");
  for (i=0; i<ROMpages(ROMsize); i++)
  {
    if ((i & 63) == 0) fprintf(stderr,"\n");
    j = rompage[i];
    if (j > 36) j = 36;
    fprintf(stderr,"%c",sym[j]);
  }
  fprintf(stderr,"\n");
}

/* ------------------------------------------------------------------------ */

void dbtrace_DApages(int DAN, uint32 DAbase, int DAsize)
{
  uint32     a,pa,pn,ppl;
  uint32     *camptr = NULL;
  camentry_t *cam;

  _swix(OS_ReadSysInfo, _INR(0,2) | _OUT(2),
        6, 0, OSRSI6_CamEntriesPointer,
        &camptr);
  if (camptr == NULL)
  {
    cam = (camentry_t *)CAMstart; /* Pre Ursula hardwired number */
  }
  else
  {
    cam = (camentry_t *)*camptr;
  }

  fprintf(stderr,"patchDA num=%8.8x size=%8.8x base=%8.8x\n",DAN,DAsize,DAbase);

  for (a=DAbase; a<DAbase+DAsize; a+=0x1000)
  {
    pa  = logtophys(a);
    pn  = logtopagenum(a);
    ppl = cam[pn].PPL;
    fprintf(stderr,"  DA log=%8.8x phys=%8.8x pnum=%8.8x PPL=%8.8x\n",a,pa,pn,ppl);
  }
}

/* ------------------------------------------------------------------------ */

#endif /* DEBUG */

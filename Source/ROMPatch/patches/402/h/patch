/* Copyright 2013 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* patches/402/patch.h */

/* sum of patches for RISC OS 4.02 */

#include "patches/402/ROMcrc.h"
#include "patches/402/CLIBpatch.h"
#include "patches/402/RESLVpatch.h"
#include "patches/402/WIMPpatch.h"
#include "patches/402/MSGSpatch.h"
#include "patches/402/CDSFTpatch.h"
#include "patches/402/DMANpatch.h"
#include "patches/402/PINBpatch.h"
#include "patches/402/DESKpatch.h"
#include "patches/402/DRWFpatch.h"
#include "patches/402/MTRANpatch.h"
#include "patches/402/ADFSpatch.h"


static patchlist_proc patchlist402[] =
{
  CLIBpatches402_proc,
  RESLVpatches402_proc,
  DMANpatches402_proc,
  DESKpatches402_proc,
  DRWFpatches402_proc,
  WIMPpatches402_proc,
  MSGSpatches402_proc,
  CDSFTpatches402_proc,
  PINBpatches402_proc,
  MTRANpatches402_proc,
  ADFSpatches402_proc,
  NULL
};

static ROMentry_t ROMentry402 =
{
  romcrc402,
  1,                     /* 4.02 supports ROM-space write protect */
  0,                     /* 4.02 checksum is wrong */
  4,                     /* 4.02 comes on 4MB of ROM */
  M_name402,
  patchlist402
};

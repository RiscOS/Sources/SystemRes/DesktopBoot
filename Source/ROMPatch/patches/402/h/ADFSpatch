/* Copyright 2018 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* patches/402/ADFSpatch.h */

static patchentry_t *ADFSpatches402_proc(void *handle)
{

  static patchentry_t ADFSpatches402[] =
  {
  /*            addr        old         new */

     /* Disc error 20 hook */
     {(uint32 *)0x039C1748, 0x03A00020, 0x0A08B34B},
     {(uint32 *)0x039C174C, 0x08BD437E, 0xE1A00000},
     {(uint32 *)0x039C1750, 0x039EF201, 0xE1A00000},

     /* Disc error 20 patch */
     {(uint32 *)0x03bee47c, 0xffffffff, 0xE3A0E302},
     {(uint32 *)0x03bee480, 0xffffffff, 0xE11E000F},
     {(uint32 *)0x03bee484, 0xffffffff, 0x013EF00F},
     {(uint32 *)0x03bee488, 0xffffffff, 0x13A0E000},
     {(uint32 *)0x03bee48c, 0xffffffff, 0xE59C1220},
     {(uint32 *)0x03bee490, 0xffffffff, 0xE59F2098},
     {(uint32 *)0x03bee494, 0xffffffff, 0xE5821000},
     {(uint32 *)0x03bee498, 0xffffffff, 0xE3A01001},
     {(uint32 *)0x03bee49c, 0xffffffff, 0xE58C1220},
     {(uint32 *)0x03bee4a0, 0xffffffff, 0xE28F1008},
     {(uint32 *)0x03bee4a4, 0xffffffff, 0xE58C1224},
     {(uint32 *)0x03bee4a8, 0xffffffff, 0xE13EF00F},
     {(uint32 *)0x03bee4ac, 0xffffffff, 0xE8BD837E},
     {(uint32 *)0x03bee4b0, 0xffffffff, 0xE92D4001},
     {(uint32 *)0x03bee4b4, 0xffffffff, 0xE1A0000F},
     {(uint32 *)0x03bee4b8, 0xffffffff, 0xE33FF3C2},
     {(uint32 *)0x03bee4bc, 0xffffffff, 0xE1A00000},
     {(uint32 *)0x03bee4c0, 0xffffffff, 0xE92D49FF},
     {(uint32 *)0x03bee4c4, 0xffffffff, 0xE59CB228},
     {(uint32 *)0x03bee4c8, 0xffffffff, 0xE59F3060},
     {(uint32 *)0x03bee4cc, 0xffffffff, 0xE5932000},
     {(uint32 *)0x03bee4d0, 0xffffffff, 0xE2522001},
     {(uint32 *)0x03bee4d4, 0xffffffff, 0x0A00000F},
     {(uint32 *)0x03bee4d8, 0xffffffff, 0xE5DB0818},
     {(uint32 *)0x03bee4dc, 0xffffffff, 0xE3100008},
     {(uint32 *)0x03bee4e0, 0xffffffff, 0x05832000},
     {(uint32 *)0x03bee4e4, 0xffffffff, 0x03A02001},
     {(uint32 *)0x03bee4e8, 0xffffffff, 0x0A000005},
     {(uint32 *)0x03bee4ec, 0xffffffff, 0xE59C1208},
     {(uint32 *)0x03bee4f0, 0xffffffff, 0xE59C01E8},
     {(uint32 *)0x03bee4f4, 0xffffffff, 0xE1A0E00F},
     {(uint32 *)0x03bee4f8, 0xffffffff, 0xE59CF240},
     {(uint32 *)0x03bee4fc, 0xffffffff, 0xE59F2028},
     {(uint32 *)0x03bee500, 0xffffffff, 0xE58C2224},
     {(uint32 *)0x03bee504, 0xffffffff, 0xE58C2220},
     {(uint32 *)0x03bee508, 0xffffffff, 0xE8BD49FF},
     {(uint32 *)0x03bee50c, 0xffffffff, 0xE13FF000},
     {(uint32 *)0x03bee510, 0xffffffff, 0xE1A00000},
     {(uint32 *)0x03bee514, 0xffffffff, 0xE8FD8001},
     {(uint32 *)0x03bee518, 0xffffffff, 0xE3A00020},
     {(uint32 *)0x03bee51c, 0xffffffff, 0xE59C21D4},
     {(uint32 *)0x03bee520, 0xffffffff, 0xE59C31F4},
     {(uint32 *)0x03bee524, 0xffffffff, 0xE5DC71BC},
     {(uint32 *)0x03bee528, 0xffffffff, 0xEAF74CFD},
     {(uint32 *)0x03bee52c, 0xffffffff, 0x039C19A0},
     {(uint32 *)0x03bee530, 0xffffffff, 0x00000000}, /* Patched with adfs354_word_addr below */

     {(uint32 *)0,0,0}
  };

  ADFSpatches402[(sizeof(ADFSpatches402)/sizeof(patchentry_t))-2].newval = adfs354_word_addr;

  return ADFSpatches402;
}

Flow
====

1. Start
The boot drive runs '!Boot' (ie. *OPT4,2), this in turn runs '!Boot.!Run'.

2. !Boot.!Run
Check not pre RISC OS 3.10.
Run '!System' (this loads a newer C library, FPEmulator, and CallASWI as needed).
It is now safe to run C code compiled as APCS-32.
BootVars sets up various 'Boot$' variables.
If this was run from the desktop (ie. double clicking on !Boot) '!Boot.Utils.DeskRun' is called.
If this was run from outside the desktop '!Boot.Utils.BootRun' is called.

3. !Boot.!Boot
This is ignored by the boot sequence, but will be seen by the filer later.
If the boot sequence is not run then on first being seen by the filer an error message will be shown reminding the user that stuff might not work.

3. !Boot.Utils.DeskRun
Selects a version of !Configure appropriate to the current OS.
Flow ends here.

4. !Boot.Utils.BootRun
Looks for a !Territory application.
Sets up some aliases for the pre-desktop section of the boot sequence.

5. !Boot.Utils.SetChoices (from !Boot.Utils.BootRun) 
Defines the 'Choices$' variables, creating a choices directory if absent, populating it will a default set of files from ROxxxHook.

6. Choices:Boot.PreDesktop (from !Boot.Utils.BootRun)

7. !Boot.Utils.BootRun
Check for the existence of a SoftLoad resource, if there is one, run it.
Set up a scrap directory.
Load (using Alias$@LoadType for files accepted during the boot sequence) files from the Choices:Boot.Predesk directory.
Run directories from the Choices:Boot.Predesk directory.
Unset the predesktop aliases.
Enter the desktop. If the boot sequence has succeeded in running to this point the desktop will be entered regardless of the setting of the configured language. In the case of an error prior to this point, the default language is entered.

8. Choices:Boot.Desktop
By virtue of Choices:Boot.Desktop using the filetype &FEA the desktop is entered because the Alias$@RunType issues a *DESKTOP command.
Filer boots any readonly resources in the ROxxxHook(s).
Filer boots any readonly resources in the !Boot.Resources directory.
Filer boots !Boot itself.
Filer runs everything from the Choices:Boot.Tasks directory, launching each one as a separate task.
Filer boots expected locations of applications in the standard disc image, if they exist, and any added specific ones added by the user.

9. Boot:Utils.HOff (from Choices:Boot.Desktop)
This doesn't use the *HOFF command, the hourglass must remain on until the desktop is idle, and the preceding steps in Choices:Boot.Desktop have potentially queued up a lot of things that need doing or that may take a while, whereas the *HOFF command in the Hourglass module is instant. 

10. Choices:Boot.Desktop
Some tidying up in case font/icons have changed, repointing aliases now that !Configure has been seen.
Flow ends here.

Reasons for modules inside !System
==================================
!System.310.Modules.310Support.icons
!System.310.Modules.310Support.icons22
!System.310.Modules.310Support.ThreeTen
  Support module & sprites for the Toolbox on systems with Wimp < 3.50.
  Loaded by ColourDbox and Window modules in the Toolbox, see Appendix B of ISBN 1852501650.
!System.310.Modules.ABCLib
  Support module for applications written in BASIC compiled with Pineapple software's compiler.
  Not loaded by the boot sequence.
!System.310.Modules.BASIC
!System.310.Modules.BASIC64
  Softloadable BASIC module.
  Not loaded by the boot sequence.
!System.310.Modules.BasicEdit
  The ARM BASIC editor, started by BASIC in response to the EDIT keyword.
  Not loaded by the boot sequence.
!System.310.Modules.BootCmds
  Extra utility commands for the boot sequence.
  Loaded by !Boot.Utils.BootRun for the commands previously provided on disc in !Boot.Utils and !Boot.Library for the boot sequence.
!System.310.Modules.BorderUtil
  Patch module to allow 3D window tool buttons with Wimp 3.10.
  Loaded by ThreeTen, and explicitly by some applications.
  Must be killed if a Wimp > 3.16 is softloaded (BorderUtils checks this itself, but only when loaded).
!System.310.Modules.CallASWI
  Provides OS_CallASWI/OS_CallASWIR12 for the CLib, and the other new OS_ SWIs added from RISC OS 3.70 to 5.
  Loaded by CLib, and explicitly by some applications.
!System.310.Modules.CLib
  Updated SharedCLibrary with APCS-R and APCS-32 calling conventions.
  Loaded by !Boot.!Run so that the boot sequence can contain C programs.
!System.310.Modules.CMOSUtils
  Caches the CMOS to improve performance for versions prior to RISC OS 3.50.
  Loaded by !Boot.Utils.BootRun early enough to not have written to the CMOS yet.
!System.310.Modules.DDEUtils
  Provides longer command line support and per-application path variables primarly for C programs.
  Not loaded by the boot sequence.
!System.310.Modules.DragAnObj
  Updated copy of ROM module.
  Not loaded by the boot sequence.
!System.310.Modules.DragASprit
  Updated copy of ROM module.
  Loaded by ThreeTen, and explicitly by some applications.
!System.310.Modules.DrawFile
  Updated copy of ROM module.
  Not loaded by the boot sequence.
!System.310.Modules.FilerUtils
  Patch module to stop Filer exploding when a ShareFS share comes/goes.
  Loaded during PreDesk.
!System.310.Modules.FilterMgr
  Updated copy of ROM module.
  Not loaded by the boot sequence.
!System.310.Modules.FPEmulator
  Floating point for machines with varying levels of FPA instruction set support.
  Loaded by CLib, and explicitly by some applications.
!System.310.Modules.FrontEnd
  Supporting module for scriptable front ends primarily for launching command line programs.
  Not loaded by the boot sequence.
!System.310.Modules.Installer
  Supporting module to install and merge software updates.
  Loaded by !Configure for BootMerge/FontMerge/SysMerge, and explicitly by some applications.
!System.310.Modules.JCompMod
  JPEG compressor, used by !ChangeFSI and some other applications.
  Not loaded by the boot sequence.
!System.310.Modules.Network.AUNMsgs
  Common messages for Acorn universal networking.
  Loaded by !Boot.Resources.!Internet.!Run (via !Boot.Choices.Boot.SetUpNet) if not in ROM.
!System.310.Modules.Network.DHCP
  Dynamic Host Configuration Protocol.
  Loaded by !Boot.Resources.!Internet.!Run (via !Boot.Choices.Boot.SetUpNet) if not in ROM and DHCP is used to set the network address.
!System.310.Modules.Network.Freeway
!System.310.Modules.Network.ShareFS
  The Acorn Access protocol.
  Loaded by !Boot.Resources.!Internet.!Run (via !Boot.Choices.Boot.SetUpNet) if not in ROM and Access is enabled.
!System.310.Modules.Network.Internet
  The TCP/IP stack.
  Loaded by !Boot.Resources.!Internet.!Run (via !Boot.Choices.Boot.SetUpNet) if not in ROM.
!System.310.Modules.Network.MimeMap
  File type to extension to MIME type database.
  Loaded by !Boot.Utils.BootRun if not in ROM, regardless of whether networking is enabled or not.
!System.310.Modules.Network.MManager
  IP packet memory manager.
  Loaded by !Boot.Resources.!Internet.!Run (via !Boot.Choices.Boot.SetUpNet) if not in ROM.
!System.310.Modules.Network.NetI
  Econet-over-IP module.
  Loaded by !Boot.Choices.Internet.Startup when AUN is enabled from !InetSetup.
!System.310.Modules.Network.PPP
  Point to point protocol.
  Loaded by !Boot.Choices.Internet.Startup when PPP is selected as the driver from !InetSetup.
!System.310.Modules.Network.Resolver
  Converts domain names to network addresses.
  Loaded by !Boot.Resources.!Internet.!Run (via !Boot.Choices.Boot.SetUpNet) if not in ROM.
!System.310.Modules.Network.RouteD
  Routing daemon runs in the background and keeps routing tables up to date.
  Loaded when 'Run RouteD' is selected in the Routing dialogue of !InetSetup.
!System.310.Modules.Network.URI
  Centralised URI manager, looks after the various fetchers.
  Not loaded by the boot sequence.
!System.310.Modules.Network.URL.AcornHTTP
  Centralised HTTP fetcher.
  Not loaded by the boot sequence.
!System.310.Modules.Network.URL.URL
  Centralised URL fetcher.
  Not loaded by the boot sequence.
!System.310.Modules.Picker
  Updated copy of ROM module.
  Not loaded by the boot sequence.
!System.310.Modules.RedrawMgr
  Updated copy of ROM module.
  Not loaded by the boot sequence.
!System.310.Modules.SerialUtil
  Patch module to try to emulate buffer vector behaviour from RISC OS 2.00, mentioned in the A5000 release note for RISC OS 3.10. Only use this if an old serial port application does not function correctly.
  Not loaded by the boot sequence.
!System.310.Modules.SSound
  Multi channel software sound mixer.
  Not loaded by the boot sequence.
!System.310.Modules.TaskWindow
  Updated copy of ROM module.
  Not loaded by the boot sequence.
!System.310.Modules.Toolbox.ColourDbox
!System.310.Modules.Toolbox.ColourMenu
!System.310.Modules.Toolbox.DCS_Quit
!System.310.Modules.Toolbox.FileInfo
!System.310.Modules.Toolbox.FontDbox
!System.310.Modules.Toolbox.FontMenu
!System.310.Modules.Toolbox.Iconbar
!System.310.Modules.Toolbox.Menu
!System.310.Modules.Toolbox.PrintDbox
!System.310.Modules.Toolbox.ProgInfo
!System.310.Modules.Toolbox.SaveAs
!System.310.Modules.Toolbox.Scale
!System.310.Modules.Toolbox.Tabs
!System.310.Modules.Toolbox.ToolAction
!System.310.Modules.Toolbox.Toolbox
!System.310.Modules.Toolbox.TreeView
!System.310.Modules.Toolbox.Window
  Updated copy of the Toolbox.
  Loaded by !Boot.Resources.!System.!Run and formerly by UnplugTBox before any toolbox clients are active.
!System.310.Modules.Wimp
  Updated copy of ROM module.
  Loaded by !Boot.Choices.Boot.PreDesktop if required.
!System.310.Modules.WimpMan
  Support module for applications to abstract some Wimp features typically related to Pineapple software's products.
  Not loaded by the boot sequence.
!System.310.Modules.ZLib
  Implementation of the popular ZLib compression tools.
  Not loaded by the boot sequence.
!System.350.Modules.CompressPNG
  PNG compressor, used by !ChangeFSI and some other applications.
  Not loaded by the boot sequence.
!System.350.Modules.Fonts
  Blending font manager.
  Loaded by !Boot.Choices.Boot.PreDesk.FontBlend which originates in RO350Hook.
!System.350.Modules.FPEmulator
  Floating point for machines with varying levels of FPA instruction set support.
  Loaded by CLib, and explicitly by some applications.
!System.350.Modules.ITable
  Inverse font dithering calculator, to support blending font manager.
  Loaded by !Boot.Choices.Boot.PreDesk.FontBlend which originates in RO350Hook.
!System.350.Modules.Network.URL.AcornSSL
  Security module.
  Not loaded by the boot sequence.
!System.350.Modules.Toolbox.TextGadget
  Updated copy of the Toolbox.
  Loaded by !Boot.Resources.!System.!Run and formerly by UnplugTBox before any toolbox clients are active.
!System.350.Modules.Wimp
  Updated copy of ROM module.
  Loaded by !Boot.Choices.Boot.PreDesktop if required.
!System.360.Modules.Installer
  Supporting module to install and merge software updates, this version uses internationalised help for its commands which was not supported until RISC OS 3.60.
  Loaded by !Configure for BootMerge/FontMerge/SysMerge, and explicitly by some applications.
!System.360.Modules.Network.NetTime
  Network time protocol module.
  Loaded by !Configure plugin !TimeSetup.
!System.360.Modules.Video.ScreenFX
  Various fading/screen special effects.
  Not loaded by the boot sequence.
!System.360.Modules.Wimp
  Updated copy of ROM module.
  Loaded by !Boot.Choices.Boot.PreDesktop if required.
!System.370.Modules.PatchApp
  Application patcher, originally introduced for patching non StrongARM aware applications. Contains a handful of patch signatures which can be augmented at run time.
  Loaded by !Boot.Utils.BootRun on all systems prior to RISC OS 5.
!System.370.Modules.UnSqzAIF
  Replacement AIF application decompressor.
  Not loaded by the boot sequence.
!System.370.Modules.Wimp
  Updated copy of ROM module.
  Loaded by !Boot.Choices.Boot.PreDesktop if required.
!System.400.Modules.FPEmulator
  Floating point for machines with varying levels of FPA instruction set support.
  Loaded by CLib, and explicitly by some applications.
!System.400.Modules.Wimp
  Updated copy of ROM module.
  Loaded by !Boot.Choices.Boot.PreDesktop if required.
!System.500.Modules.CLib
  Updated SharedCLibrary with only APCS-32 calling convention.
  Loaded by !Boot.!Run so that the boot sequence can contain C programs.
!System.500.Modules.FrontEnd
  Supporting module for scriptable front ends primarily for launching command line programs.
  Not loaded by the boot sequence.
!System.500.Modules.Installer
  Supporting module to install and merge software updates, this version is 32 bit compatible.
  Loaded by !Configure for BootMerge/FontMerge/SysMerge, and explicitly by some applications.
!System.500.Modules.RTSupport
  Alternative low level multitasking model.
  Loaded by !Boot.Choices.Boot.PreDesk.SoftSCSI which originates in RO500Hook and RO510Hook.
!System.500.Modules.VideoOverlay
  Manages multi plane video in the desktop.
  Not loaded by the boot sequence.

Main changes from Acorn UniBoot (part number 1215,602-03)
=========================================================
In !Boot.Library
----------------
AddApp, AppSize, Do, IfThere, LoadCMOS, Repeat, SafeLogon are all now in the BootCommands module.
This is consistent with the functional spec review (Docs.Review) section 3.12 which sought to reduce boot time and network accesses when these commands are hammered during the boot sequence.

An optional utility, PowerOff, is provided for those platforms with software controlled powersupplies. It can be deleted to save disc space if the user wishes.
 
In !Boot.SiteHook/MchConfig
---------------------------
These (empty) directories only appear on the fileserver copies of !Boot.

In !Boot.Utils
--------------
FreePool and AddToRMA is now in the BootCommands module.
CallASWI and CMOSUtils have moved into !System, previously the !System directory was not seen until later in the boot sequence so it was not possible to refer to 'System:Modules'. As !System is now seen in '!Boot.BootRun' for the C Library these modules have joined all the others.
VProtect has been retired as the commercial half of the virus protector is no longer available.
NetChoices and SetChoices have been combined into one.
UnplugTBox has been retired - unplugging ROM modules caused more trouble than it solved, now !System just RMEnsures a minimal working set of Toolbox modules when the C Library is updated. This must be done reasonably early on because clients of the toolbox can't be passed from one set of toolbox modules to another at runtime (error "Toolbox task(s) active").
PatchApp is now in Sys:370.Modules - it patches StrongARM incompatible apps, it is only loaded for versions prior to RISC OS 5.00 on the assumption that if StrongARM didn't kill you, 32 bit probably will have.
HOn and HOff - for turning on and off the hourglass. Later versions of the hourglass module include these commands by default. Note that HOff is actually a simple Wimp application, it waits for a null poll then turns off the hourglass.

Merging with RISC OS 4.0x/4.39
==============================
Skeleton support exists for these ROM releases.
The configure plugins (from RO400Hook.Res and RO430Hook.Res) will need to be copied over the top of the empty ones provided, these are not supplied for copyright reasons.

Merging with RISC OS 3.xx
=========================
Merge (using the !SysMerge obey file) the 26 bit !System resources with those already in !Boot, this adds a few missing modules that are in ROM from RISC OS 4 onwards. At some point this could be automated by combining ZIP files.
